'''
Programmer: Parsa Nikpour
Date: 28 February 2015
This program calculates the square root manually
...

'''

def sqrt2(x, debug=False):
	s = 1
	iterationMax = 100;
	tol = 10e-14
	for k in range(iterationMax):
		if debug:
			print "Before iteration %s, s = %20.15f" % (k,s)
		s0 = s;
		s = 0.5 * (s + x/s)
		delta_s = s - s0
		if (abs(delta_s / x) < tol):
			break;

	if debug:
		print "After %s iterations, s = %20.15f" % (k+1,s)
	return s
